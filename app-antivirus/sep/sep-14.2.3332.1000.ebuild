# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Symantec Endpoint Protection"
HOMEPAGE=""
SRC_URI="${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="fetch"
DEPEND="virtual/libc
	acct-group/avdefs
	dev-libs/log4cpp"
RDEPEND="${DEPEND}"
BDEPEND=""

pkg_preinst() {
	mkdir -p /var/lock/subsys/ >> /dev/null 2>&1

	if [ ! -e /etc/Symantec.conf ] ; then
		elog "Creating /etc/Symantec.conf file"
		echo "[Symantec Shared]" > /etc/Symantec.conf
		echo "BaseDir=/opt/Symantec" >> /etc/Symantec.conf
	fi

	# Stop services if needed
	if [ -f /etc/init.d/smcd ] ; then
		/etc/init.d/smcd stop >> /dev/null 2>&1
	fi

	if [ -f /etc/init.d/rtvscand ] ; then
		/etc/init.d/rtvscand stop >> /dev/null 2>&1
	fi

	if [ -f /etc/init.d/symcfgd ] ; then
		/etc/init.d/symcfgd stop >> /dev/null 2>&1
	fi

}

src_install() {
	doinitd etc/init.d/rtvscand
	doinitd etc/init.d/smcd
	doinitd etc/init.d/symcfgd

	insinto /etc
	doins -r etc/symantec
	doins -r etc/sysconfig

	insinto /opt
	doins -r opt/Symantec/

	exeinto /opt/Symantec/bin
	doexe opt/Symantec/bin/navdefutil
	
	into /opt/Symantec/symantec_antivirus
	dolib.so opt/Symantec/symantec_antivirus/*.so
	mv ${D}/opt/Symantec/symantec_antivirus/lib64/* ${D}/opt/Symantec/symantec_antivirus/
	rmdir ${D}/opt/Symantec/symantec_antivirus/lib64

	exeinto /opt/Symantec/symantec_antivirus
	doexe opt/Symantec/symantec_antivirus/rtvscand
	doexe opt/Symantec/symantec_antivirus/sadiag.sh
	doexe opt/Symantec/symantec_antivirus/sav
	doexe opt/Symantec/symantec_antivirus/smcd
	doexe opt/Symantec/symantec_antivirus/symcfg
	doexe opt/Symantec/symantec_antivirus/symcfgd
	doexe opt/Symantec/symantec_antivirus/symcfgpop
	doexe opt/Symantec/symantec_antivirus/uninstall.sh
	doexe opt/Symantec/symantec_antivirus/upgrade.sh
	
	#rm -rf ${D}/opt/Symantec/symantec_antivirus/tools
	
	#dodir var/log/symantec
	#dodir var/run/symantec
	
	keepdir var/symantec/sep/auto
	keepdir var/symantec/sep/cve
	keepdir var/symantec/sep/Logs
	keepdir var/symantec/sep/pending
	keepdir var/symantec/sep/Quarantine
	keepdir var/symantec/sep/sent

	doman usr/share/man/man1/*.1
	doman usr/share/man/man8/*.8
}

pkg_postinst() {
	# Update the library path
	if [ ! -f /etc/env.d/30symantec_antivirus ] ; then
		echo 'LDPATH="/opt/Symantec/symantec_antivirus/lib64"' >> /etc/env.d/30symantec_antivirus
	fi
	ldconfig /opt/Symantec/symantec_antivirus >> /dev/null 2>&1

	# Populate the config database
	pushd /opt/Symantec/symantec_antivirus/ >> /dev/null 2>&1
	/opt/Symantec/symantec_antivirus/symcfgpop symcfgdata.inf
	popd >> /dev/null 2>&1

	# Install and register the defs
	chmod -R g+w /opt/Symantec/virusdefs

	ca_cert_inf=/opt/Symantec/symantec_antivirus/caconfigreg.inf
	caconfig=/opt/Symantec/symantec_antivirus/caconfigreg

	# check and migrate av setting.
	AVSETTINGS=/opt/Symantec/symantec_antivirus/avregdata.inf
	if [ -f $AVSETTINGS ] ; then
			elog "need migrate AV settings."
			/opt/Symantec/symantec_antivirus/symcfgpop $AVSETTINGS
			rm -fv $AVSETTINGS
	fi

	# check and migrate lu data.
	luregdata=/opt/Symantec/symantec_antivirus/luschedulereg
	upgradesh=/opt/Symantec/symantec_antivirus/upgrade.sh
	luregdatainf=/opt/Symantec/symantec_antivirus/luregdata.inf
	if [ -f $luregdata ] ; then #need migrate lu schedule reg.
			echo "$luregdata exists, need to migrate lu data."
			if [ -f $upgradesh ] ; then
					$upgradesh
					rm -f $luregdata
					rm -f $caconfig
					if [ -f $luregdatainf ] ; then
							elog "succeed to generate lu reg data."
							if [ -f /etc/init.d/symcfgd ] ; then
									/etc/init.d/symcfgd stop
							fi
							savregutil=/opt/Symantec/symantec_antivirus/symcfgpop
							$savregutil $luregdatainf
							if [ 0 -eq $? ] ; then
									elog "succeed to migrate lu data."
									rm -fv $luregdatainf
							else
									eerror "fail to migrate lu data."
							fi
					else
							eerror "failed to generate lu reg data."
					fi
			else
					eerror "$upgradesh doesn't exist"
			fi
	fi

	#migrate ca config data if present, else create one
	if [ ! -f ${ca_cert_inf} ] ; then
			ca_file="/etc/symantec/sep/sepfl.pem"

			#now create a reg file to import
			elog "Adding OS CA Certificate store to reg"
			echo "!KEY!=Symantec Endpoint Protection" >> $ca_cert_inf
			echo "CABundleFile=S$ca_file" >> $ca_cert_inf
			echo "CABundlePath=S" >> $ca_cert_inf
	fi

	/opt/Symantec/symantec_antivirus/symcfgpop $ca_cert_inf
	rm -fv $ca_cert_inf
}

pkg_prerm() {
	# Unregister with the defs
	LD_LIBRARY_PATH="/opt/Symantec/symantec_antivirus/tools/" /opt/Symantec/bin/navdefutil --uninstall SAV_LINUX

	# Stop services
	if [ -x /etc/init.d/smcd ] ; then
		if [ -x /usr/sbin/invoke-rc.d ] ; then
				invoke-rc.d smcd stop
				update-rc.d -f smcd remove >/dev/null
		else
				/etc/init.d/smcd stop
		fi
	fi
	if [ -x /etc/init.d/symcfgd ]; then
		if [ -x /usr/sbin/invoke-rc.d ]; then
			invoke-rc.d symcfgd stop
			update-rc.d -f symcfgd remove >/dev/null
		else
			/etc/init.d/symcfgd stop
		fi
	fi
	if [ -x /etc/init.d/rtvscand ]; then
		if [ -x /usr/sbin/invoke-rc.d ]; then
			invoke-rc.d rtvscand stop
			update-rc.d -f rtvscand remove >/dev/null
		else
			/etc/init.d/rtvscand stop
		fi
	fi
}

pkg_postrm() {
	elog "Performing final uninstall post-uninstall actions"

	# Remove the configuration files of SAV registry
	if [ -d /etc/symantec ]
	then
		rm -fv /etc/symantec/VPREGDB.*
	fi

	if [ -d /etc/symantec/sep ]
	then
		rm -fv /etc/symantec/sep/VPREGDB.*
	fi

	if [ -d /etc/env.d/30symantec_antivirus ]
	then
		rm -fv /etc/env.d/30symantec_antivirus;
	fi

	ldconfig
}
