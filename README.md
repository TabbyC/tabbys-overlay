# tabbys-overlay
A Gentoo ebuild repository (overlay) for my own ebuilds.

## How to use this repository?

There are two main methods for making use of this repository, discussed in the sections below.

### Local repositories

For the [local repository](https://wiki.gentoo.org/wiki/Handbook:Parts/Portage/CustomTree#Defining_a_custom_repository) method, create a `/etc/portage/repos.conf/tabbys-overlay.conf` file containing the following bit of text.

```
[tabbys-overlay]
priority = 50
location = <repo-location>/tabbys-overlay
sync-type = git
sync-uri = https://gitlab.com/TabbyC/tabbys-overlay.git
auto-sync = Yes
```

Change `repo-location` to a path of your choosing and then run `emerge --sync`, Portage should now find and update the repository.

### Layman

You can also use the Layman tool to add and sync the repository, read the instructions on the [Gentoo Wiki](https://wiki.gentoo.org/wiki/Layman#Adding_custom_repositories).

