# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit python-single-r1

PYTHON_COMPAT=( python2_7 )

DESCRIPTION="A fast build system that encourages the creation of small, reusable modules over a variety of platforms and languages."
HOMEPAGE="https://buck.build"
SRC_URI="https://github.com/facebook/${PN}/archive/v${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="watchman"

DEPEND="${PYTHON_DEPS}"
RDEPEND="${DEPEND}"
BDEPEND=">=virtual/jdk-1.8.0
	>=dev-java/ant-1.9
	dev-vcs/git
	watchman? ( sys-fs/watchman )"

